package com.vetexpert.visit.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.util.*

@Table("manipulation")
data class ManipulationEntity(
    @Id
    val id: UUID,
    val count: Int,
    val dictionaryId: UUID,
    val visitId: UUID
)
