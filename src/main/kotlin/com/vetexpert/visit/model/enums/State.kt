package com.vetexpert.visit.model.enums

enum class State {
    TEMPLATE,
    READY_FOR_PAYMENT,
    PAYED,
    CLOSED
}
