package com.vetexpert.visit.model

import com.vetexpert.visit.model.enums.State
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.util.*

@Table("visit")
data class VisitEntity(
    @Id
    var id: UUID?,
    val doctorId: UUID,
    val ownerId: UUID,
    val petId: UUID,
    val anamnesis: String?,
    val checkup: String?,
    val temporaryDiagnosis: String?,
    val finallyDiagnosis: String?,
    val recommendations: String?,
    val date: Long = Date().time,
    val state: State,
    val number: Long,
    val clinicId: UUID
)
