package com.vetexpert.visit.model

import com.vetexpert.proto.visit.VisitModel
import java.util.*

data class DaoVisitFilter(
    val stateList: List<VisitModel.State> = emptyList(),
    val ownerIdList: Set<UUID> = emptySet(),
    val dateFrom: Long = 0,
    val dateTo: Long = 0,
    val doctorIdList: List<UUID> = emptyList(),
    val diagnosis: String = "",
    val petIdList: Set<UUID> = emptySet(),
    val clinicIds: Set<UUID> = emptySet()
)
