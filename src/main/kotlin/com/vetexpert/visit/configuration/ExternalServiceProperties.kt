package com.vetexpert.visit.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("service")
data class ExternalServiceProperties(
    val dictionaryUrl: String,
    val personUrl: String,
    val petUrl: String,
    val userUrl: String
)
