package com.vetexpert.visit

import com.vetexpert.visit.configuration.ExternalServiceProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(value = [ExternalServiceProperties::class])
class VisitApplication

fun main(args: Array<String>) {
    runApplication<VisitApplication>(*args)
}
