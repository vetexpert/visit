package com.vetexpert.visit.repository

import com.vetexpert.visit.model.VisitEntity
import org.springframework.data.r2dbc.repository.R2dbcRepository
import java.util.*

interface VisitRepository : R2dbcRepository<VisitEntity, UUID>
