package com.vetexpert.visit.repository.impl

import com.vetexpert.visit.model.DaoVisitFilter
import com.vetexpert.visit.model.VisitEntity
import com.vetexpert.visit.repository.CustomVisitRepository
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate
import org.springframework.data.r2dbc.core.select
import org.springframework.data.r2dbc.query.Criteria.where
import org.springframework.data.relational.core.query.CriteriaDefinition
import org.springframework.data.relational.core.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
class CustomVisitRepositoryImpl(private val template: R2dbcEntityTemplate) : CustomVisitRepository {

    override fun findAllByFilter(filter: DaoVisitFilter): Flux<VisitEntity> =
        template.select<VisitEntity>().matching(Query.query(filterBuilder(filter))).all()

    private fun filterBuilder(
        filter: DaoVisitFilter
    ) =
        CriteriaDefinition.from(mutableListOf<CriteriaDefinition>().apply {
            add(where("clinic_id").`in`(filter.clinicIds))
            if (filter.stateList.isNotEmpty()) add(where("state").`in`(filter.stateList))
            if (filter.ownerIdList.isNotEmpty()) add(where("owner_id").`in`(filter.ownerIdList))
            if (filter.dateFrom != 0L) add(where("date").greaterThanOrEquals(filter.dateFrom))
            if (filter.dateTo != 0L) add(where("date").lessThanOrEquals(filter.dateTo))
            if (filter.doctorIdList.isNotEmpty()) add(where("doctor_id").`in`(filter.doctorIdList))
            if (filter.petIdList.isNotEmpty()) add(where("pet_id").`in`(filter.petIdList))
            if (filter.diagnosis.isNotBlank()) {
                add(where("temporary_diagnosis").like("%${filter.diagnosis}%").ignoreCase(true)
                    .or(where("finally_diagnosis").like("%${filter.diagnosis}%").ignoreCase(true))
                )
            }
        })
}
