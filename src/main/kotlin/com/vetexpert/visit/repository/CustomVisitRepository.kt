package com.vetexpert.visit.repository

import com.vetexpert.visit.model.DaoVisitFilter
import com.vetexpert.visit.model.VisitEntity
import reactor.core.publisher.Flux

interface CustomVisitRepository {
    fun findAllByFilter(filter: DaoVisitFilter): Flux<VisitEntity>
}
