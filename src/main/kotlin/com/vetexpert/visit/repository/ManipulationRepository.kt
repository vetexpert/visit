package com.vetexpert.visit.repository

import com.vetexpert.visit.model.ManipulationEntity
import org.springframework.data.r2dbc.repository.R2dbcRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

interface ManipulationRepository : R2dbcRepository<ManipulationEntity, UUID> {

    fun findAllByVisitId(visitId: UUID): Flux<ManipulationEntity>
}
