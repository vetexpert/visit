package com.vetexpert.visit.service.impl

import com.vetexpert.proto.person.PersonModel
import com.vetexpert.proto.pet.PetModel
import com.vetexpert.proto.user.UserModel
import com.vetexpert.proto.visit.VisitModel
import com.vetexpert.proto.visit.VisitModel.VisitFilter
import com.vetexpert.proto.visit.VisitModel.VisitList
import com.vetexpert.utils.uuid
import com.vetexpert.visit.client.DictionaryClient
import com.vetexpert.visit.client.PersonClient
import com.vetexpert.visit.client.PetClient
import com.vetexpert.visit.client.UserClient
import com.vetexpert.visit.model.DaoVisitFilter
import com.vetexpert.visit.model.VisitEntity
import com.vetexpert.visit.repository.CustomVisitRepository
import com.vetexpert.visit.repository.ManipulationRepository
import com.vetexpert.visit.repository.VisitRepository
import com.vetexpert.visit.service.VisitService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import reactor.kotlin.core.util.function.component1
import reactor.kotlin.core.util.function.component2
import reactor.kotlin.core.util.function.component3
import reactor.kotlin.core.util.function.component4
import java.util.*

@Service
class VisitServiceImpl(
    private val customVisitRepository: CustomVisitRepository,
    private val petClient: PetClient,
    private val personClient: PersonClient,
    private val userClient: UserClient,
    private val manipulationRepository: ManipulationRepository,
    private val dictionaryClient: DictionaryClient,
    private val visitRepository: VisitRepository
) : VisitService {

    override fun getAll(filter: VisitFilter, clinicUUIDs: Set<UUID>): Mono<VisitList> =
        Mono.zip(
            petClient.getByName(filter.petName),
            personClient.getByFioOrPhone(filter.fio, filter.phone)
        ).flatMap { (pet, owner) ->
            if (filter.petName.isNotBlank() && pet.petList.isEmpty()) return@flatMap Mono.empty()
            if ((filter.fio.isNotBlank() || filter.phone.isNotBlank()) && owner.personList.isEmpty()) return@flatMap Mono.empty()
            DaoVisitFilter(
                stateList = filter.stateList,
                ownerIdList = owner.personList.map { it.id.uuid() }.toSet(),
                dateFrom = filter.dateFrom,
                dateTo = filter.dateTo,
                doctorIdList = filter.doctorIdList.map { it.uuid() },
                diagnosis = filter.diagnosis,
                petIdList = pet.petList.map { it.id.uuid() }.toSet(),
                clinicIds = clinicUUIDs
            ).toMono()
        }
            .flatMap { daoVisitFilter ->
                customVisitRepository.findAllByFilter(daoVisitFilter)
                    .collectList()
                    .flatMap { visitEntityList ->
                        val doctorIdList = visitEntityList.map { it.doctorId }.toSet()
                        val ownerIdList = visitEntityList.map { it.ownerId }.toSet()
                        val petIdList = visitEntityList.map { it.petId }.toSet()
                        Mono.zip(
                            petClient.getByIds(petIdList),
                            personClient.getByIds(ownerIdList),
                            userClient.getByIds(doctorIdList),
                        ).log().map { (petListResponse, personListResponse, userListResponse) ->
                            val petById = petListResponse.petList.associateBy { it.id.uuid() }
                            val personById = personListResponse.personList.associateBy { it.id.uuid() }
                            val doctorById = userListResponse.userList.associateBy { it.id.uuid() }
                            VisitList.newBuilder().addAllVisit(
                                visitEntityList.map {
                                    visitBuilderShortInfo(
                                        it,
                                        doctorById[it.doctorId],
                                        personById[it.ownerId],
                                        petById[it.petId]
                                    )
                                }
                            ).build()
                        }
                    }
            }
            .defaultIfEmpty(VisitList.getDefaultInstance())

    override fun getById(id: UUID) =
        visitRepository.findById(id)
            .flatMap {
                Mono.zip(
                    petClient.getByIds(setOf(it.petId)),
                    personClient.getByIds(setOf(it.ownerId)),
                    userClient.getByIds(setOf(it.doctorId)),
                    getManipulationListByVisitEntityId(it.id!!)
                ).map { (petListResponse, personListResponse, userListResponse, manipulationList) ->
                    visitBuilderFullInfo(it, petListResponse.petList.first(), personListResponse.personList.first(), userListResponse.userList.first(), manipulationList)
                }
            }

    private fun getManipulationListByVisitEntityId(visitEntityId: UUID): Mono<List<VisitModel.Manipulation>> =
        manipulationRepository.findAllByVisitId(visitEntityId)
            .collectList()
            .flatMap { manipulationEntityList ->
                dictionaryClient.getByIds(manipulationEntityList.map { it.dictionaryId }.toSet())
                    .map { dictionaryList ->
                        val dictionaryById = dictionaryList.dictionaryList.associateBy { it.id.uuid() }
                        manipulationEntityList.map { manipulationEntity ->
                            VisitModel.Manipulation.newBuilder().apply {
                                id = manipulationEntity.id.toString()
                                name = dictionaryById[manipulationEntity.dictionaryId]?.name
                                unit = dictionaryById[manipulationEntity.dictionaryId]?.unit
                                price = dictionaryById[manipulationEntity.dictionaryId]?.price ?: 0F
                                type = dictionaryById[manipulationEntity.dictionaryId]?.type
                                count = manipulationEntity.count
                                visitId = manipulationEntity.visitId.toString()
                            }.build()
                        }
                    }
            }
            .defaultIfEmpty(emptyList())

    private fun visitBuilderShortInfo(
        visitEntity: VisitEntity,
        doctorModel: UserModel.User?,
        personModel: PersonModel.Person?,
        petModel: PetModel.Pet?,
    ) = VisitModel.Visit.newBuilder().apply {
        uuid = visitEntity.id.toString()
        doctor = doctorModel ?: UserModel.User.getDefaultInstance()
        owner = personModel ?: PersonModel.Person.getDefaultInstance()
        pet = petModel ?: PetModel.Pet.getDefaultInstance()
        date = visitEntity.date
        state = VisitModel.State.valueOf(visitEntity.state.name)
        number = visitEntity.number
    }
        .addAllManipulation(emptyList())
        .build()

    private fun visitBuilderFullInfo(
        visitEntity: VisitEntity,
        petModel: PetModel.Pet?,
        person: PersonModel.Person?,
        user: UserModel.User?,
        manipulationEntityList: List<VisitModel.Manipulation>
    ): VisitModel.Visit =
        VisitModel.Visit.newBuilder().apply {
            uuid = visitEntity.id.toString()
            doctor = user ?: UserModel.User.getDefaultInstance()
            owner = person ?: PersonModel.Person.getDefaultInstance()
            pet = petModel ?: PetModel.Pet.getDefaultInstance()
            anamnesis = visitEntity.anamnesis ?: ""
            checkup = visitEntity.checkup ?: ""
            temporaryDiagnosis = visitEntity.temporaryDiagnosis ?: ""
            finallyDiagnosis = visitEntity.finallyDiagnosis ?: ""
            recommendations = visitEntity.recommendations ?: ""
            date = visitEntity.date
            state = VisitModel.State.valueOf(visitEntity.state.name)
            number = visitEntity.number
        }
            .addAllManipulation(manipulationEntityList)
            .build()
}
