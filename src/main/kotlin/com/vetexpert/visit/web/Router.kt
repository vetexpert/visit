package com.vetexpert.visit.web

import com.vetexpert.visit.web.handler.VisitHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.router

@Configuration
class Router(private val visitHandler: VisitHandler) {

    @Bean
    fun route() = router {
        accept(MediaType.ALL).nest {
            "visit".nest {
                POST("/list/short", visitHandler::getAll)
                GET("/{id}", visitHandler::getById)
            }
        }
    }
}
