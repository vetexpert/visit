package com.vetexpert.visit.web.handler

import com.vetexpert.proto.visit.VisitModel
import com.vetexpert.utils.CLINIC_IDS_HEADER
import com.vetexpert.utils.findAllUuids
import com.vetexpert.utils.hide
import com.vetexpert.utils.uuid
import com.vetexpert.visit.service.VisitService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

@Component
class VisitHandler(private val visitService: VisitService) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    fun getAll(request: ServerRequest): Mono<ServerResponse> =
        request.bodyToMono(ByteArray::class.java)
            .flatMap {
                VisitModel.VisitFilter.parseFrom(it).run {
                    logger.info("${request.path()}. filter: ${this.toString().lines().joinToString(" ")}")
                    visitService.getAll(this, request.headers().header(CLINIC_IDS_HEADER).first().findAllUuids())
                        .flatMap { list ->
                            logger.info("${request.method()} ${request.path()}. Response: ${list.hide()}")
                            ServerResponse.ok().bodyValue(list)
                        }
                }
            }.onErrorResume {
                it.printStackTrace()
                ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).bodyValue(it.message ?: "")
            }

    fun getById(request: ServerRequest): Mono<ServerResponse> {
        logger.info("${request.method()}: ${request.path()}.")
        val id = request.pathVariable("id").uuid()
        return visitService.getById(id)
            .flatMap {
                logger.info("${request.method()} ${request.path()}. Response: ${it.hide()}")
                ServerResponse.ok().bodyValue(it)
            }
    }
}
