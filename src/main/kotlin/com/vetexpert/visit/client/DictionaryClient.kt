package com.vetexpert.visit.client

import com.vetexpert.proto.dictionary.DictionaryModel.DictionaryList
import com.vetexpert.visit.configuration.ExternalServiceProperties
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import java.util.*

@Component
data class DictionaryClient(private val externalServiceProperties: ExternalServiceProperties) {

    private val client = WebClient.builder().baseUrl(externalServiceProperties.dictionaryUrl).build()


    fun getByIds(ids: Set<UUID>): Mono<DictionaryList> =
        client.get()
            .uri("/dictionary/list?ids=$ids")
            .retrieve()
            .bodyToMono(DictionaryList::class.java)

}
