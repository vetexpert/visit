package com.vetexpert.visit.client

import com.vetexpert.proto.user.UserModel
import com.vetexpert.proto.user.UserModel.User
import com.vetexpert.visit.configuration.ExternalServiceProperties
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.DefaultUriBuilderFactory
import reactor.core.publisher.Mono
import java.net.URI
import java.util.*

@Component
data class UserClient(private val externalServiceProperties: ExternalServiceProperties) {

    private val client = WebClient
        .builder()
        .uriBuilderFactory(DefaultUriBuilderFactory(externalServiceProperties.userUrl))
        .build()

    fun getByIds(ids: Set<UUID>): Mono<UserModel.UserList> =
        client.get()
            .uri("/user/list?ids=$ids")
            .retrieve()
            .bodyToMono(UserModel.UserList::class.java)
            .defaultIfEmpty(UserModel.UserList.getDefaultInstance())

    fun getById(userId: UUID): Mono<User> =
        client.get()
            .uri("/user/$userId")
            .retrieve()
            .bodyToMono(User::class.java)
            .defaultIfEmpty(User.getDefaultInstance())
}
