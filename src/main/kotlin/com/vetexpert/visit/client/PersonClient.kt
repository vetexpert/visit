package com.vetexpert.visit.client

import com.vetexpert.proto.person.PersonModel
import com.vetexpert.proto.person.PersonModel.Person
import com.vetexpert.proto.pet.PetModel
import com.vetexpert.visit.configuration.ExternalServiceProperties
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.DefaultUriBuilderFactory
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.*

@Component
data class PersonClient(private val externalServiceProperties: ExternalServiceProperties) {

    private val client = WebClient.builder().uriBuilderFactory(
        DefaultUriBuilderFactory(externalServiceProperties.personUrl)
    ).build()

    fun getByFioOrPhone(fio: String, phone: String): Mono<PersonModel.PersonList> =
        if (fio.isBlank() && phone.isBlank()) {
            PersonModel.PersonList.getDefaultInstance().toMono()
        } else {
            client.get()
                .uri("/person/search?${queryParamsBuilder(fio, phone)}")
                .retrieve()
                .bodyToMono(PersonModel.PersonList::class.java)
                .defaultIfEmpty(PersonModel.PersonList.getDefaultInstance())
        }

    fun getByIds(ids: Set<UUID>): Mono<PersonModel.PersonList> =
        client.get()
            .uri("/person/list?ids=$ids")
            .retrieve()
            .bodyToMono(PersonModel.PersonList::class.java)
            .defaultIfEmpty(PersonModel.PersonList.getDefaultInstance())

    fun getById(id: UUID): Mono<Person> =
        client.get()
            .uri("/person/$id")
            .retrieve()
            .bodyToMono(Person::class.java)
            .defaultIfEmpty(Person.getDefaultInstance())

    private fun queryParamsBuilder(fio: String, phone: String): String {
        val queryParams = mutableListOf<String>()
        if (fio.isNotBlank()) queryParams.add("fio=$fio")
        if (phone.isNotBlank()) queryParams.add("phone=$phone")
        return queryParams.joinToString("&").takeIf { it.isNotBlank() } ?: ""
    }
}
