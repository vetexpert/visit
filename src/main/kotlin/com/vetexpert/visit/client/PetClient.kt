package com.vetexpert.visit.client

import com.vetexpert.proto.person.PersonModel
import com.vetexpert.proto.pet.PetModel.Pet
import com.vetexpert.proto.pet.PetModel.PetList
import com.vetexpert.visit.configuration.ExternalServiceProperties
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.*

@Component
data class PetClient(private val externalServiceProperties: ExternalServiceProperties) {

    private val client = WebClient.builder().baseUrl(externalServiceProperties.petUrl).build()

    fun getByName(name: String): Mono<PetList> =
        name.takeIf { name.isNotBlank() }?.let {
            client
                .get()
                .uri("/pet/byName/$name")
                .retrieve().bodyToMono(PetList::class.java)
                .defaultIfEmpty(PetList.getDefaultInstance())
        } ?: PetList.getDefaultInstance().toMono()

    fun getByIds(ids: Set<UUID>): Mono<PetList> =
        client.get()
            .uri("/pet/list?ids=$ids")
            .retrieve()
            .bodyToMono(PetList::class.java)
            .defaultIfEmpty(PetList.getDefaultInstance())

    fun getById(petId: UUID): Mono<Pet> {
        TODO("Not yet implemented")
    }
}
