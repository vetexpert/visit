package com.vetexpert.visit.web.handler

import com.ninjasquad.springmockk.MockkBean
import com.vetexpert.proto.visit.VisitModel
import com.vetexpert.visit.service.VisitService
import com.vetexpert.visit.visitListProto
import com.vetexpert.visit.web.Router
import io.mockk.every
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBody
import reactor.core.publisher.Mono

@ExtendWith(SpringExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebFluxTest
@Import(Router::class, VisitHandler::class)
@ActiveProfiles("test")
class VisitHandlerTest {

    @MockkBean
    private lateinit var visitService: VisitService

    @Autowired
    private lateinit var webClient: WebTestClient

    @Test
    fun `get all by filter - empty filter - success`() {
        every { visitService.getAll(any(), any()) } returns Mono.just(visitListProto)

        webClient.post().uri("/visit/list/short")
            .bodyValue(VisitModel.VisitFilter.getDefaultInstance())
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectBody<ByteArray>().isEqualTo(visitListProto.toByteArray())
    }
}
