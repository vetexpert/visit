package com.vetexpert.visit.repository.impl

import com.vetexpert.visit.model.DaoVisitFilter
import com.vetexpert.visit.model.VisitEntity
import com.vetexpert.visit.model.enums.State
import com.vetexpert.visit.repository.CustomVisitRepository
import com.vetexpert.visit.repository.VisitRepository
import org.apache.commons.lang3.RandomStringUtils
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import reactor.kotlin.test.test
import java.util.*

@SpringBootTest
@ActiveProfiles("test")
class VisitRepositoryImplTest {

    @Autowired
    lateinit var customVisitRepository: CustomVisitRepository

    @Autowired
    lateinit var visitRepository: VisitRepository

    private val clinicId = UUID.randomUUID()

    @Test
    fun findAllByFilter() {
        val visitEntity = visitEntityBuilder()
        visitRepository.save(visitEntity).test().expectNextCount(1).verifyComplete()
        customVisitRepository.findAllByFilter(DaoVisitFilter(clinicIds = setOf(clinicId))).test()
            .assertNext {
                assert(visitEntity.id == it.id)
            }
            .verifyComplete()
    }

    private fun visitEntityBuilder() = VisitEntity(
        id = null,
        doctorId = UUID.randomUUID(),
        ownerId = UUID.randomUUID(),
        petId = UUID.randomUUID(),
        anamnesis = RandomStringUtils.randomAlphabetic(30),
        checkup = RandomStringUtils.randomAlphabetic(30),
        temporaryDiagnosis = RandomStringUtils.randomAlphabetic(30),
        finallyDiagnosis = RandomStringUtils.randomAlphabetic(30),
        recommendations = RandomStringUtils.randomAlphabetic(30),
        state = State.PAYED,
        number = RandomStringUtils.randomNumeric(2).toLong(),
        clinicId = clinicId
    )
}
