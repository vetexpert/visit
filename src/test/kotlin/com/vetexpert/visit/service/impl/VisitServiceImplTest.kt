package com.vetexpert.visit.service.impl

import com.vetexpert.proto.person.PersonModel
import com.vetexpert.proto.pet.PetModel
import com.vetexpert.proto.user.UserModel
import com.vetexpert.proto.visit.VisitModel
import com.vetexpert.utils.uuid
import com.vetexpert.visit.absentPerson
import com.vetexpert.visit.absentPet
import com.vetexpert.visit.client.DictionaryClient
import com.vetexpert.visit.client.PersonClient
import com.vetexpert.visit.client.PetClient
import com.vetexpert.visit.client.UserClient
import com.vetexpert.visit.doctor1
import com.vetexpert.visit.model.DaoVisitFilter
import com.vetexpert.visit.person1
import com.vetexpert.visit.personList
import com.vetexpert.visit.pet1
import com.vetexpert.visit.petList
import com.vetexpert.visit.repository.CustomVisitRepository
import com.vetexpert.visit.repository.ManipulationRepository
import com.vetexpert.visit.repository.VisitRepository
import com.vetexpert.visit.service.VisitService
import com.vetexpert.visit.visitEntity1
import com.vetexpert.visit.visitEntity2
import io.mockk.every
import io.mockk.mockk
import org.apache.commons.lang3.RandomStringUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.mockito.junit.jupiter.MockitoExtension
import reactor.core.publisher.Flux
import reactor.kotlin.core.publisher.toMono
import reactor.kotlin.test.test
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

@ExtendWith(MockitoExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class VisitServiceImplTest {

    lateinit var customVisitRepository: CustomVisitRepository
    lateinit var petClient: PetClient
    lateinit var personClient: PersonClient
    lateinit var userClient: UserClient
    lateinit var manipulationRepository: ManipulationRepository
    lateinit var dictionaryClient: DictionaryClient
    lateinit var visitRepository: VisitRepository

    lateinit var visitService: VisitService

    private val expectedDiagnosis = RandomStringUtils.randomAlphabetic(15)
    private val instantNow = Instant.now()
    private val clinicIdSet = setOf(UUID.randomUUID())

    @BeforeAll
    fun setUp() {
        customVisitRepository = mockk()
        petClient = mockk()
        personClient = mockk()
        userClient = mockk()
        manipulationRepository = mockk()
        dictionaryClient = mockk()
        visitRepository = mockk()
        initMock()
        visitService = VisitServiceImpl(
            customVisitRepository,
            petClient,
            personClient,
            userClient,
            manipulationRepository,
            dictionaryClient,
            visitRepository
        )
    }

    @ParameterizedTest(name = "get all visits - success - filter = {0}")
    @MethodSource("visitFilterProvider")
    fun `get all visits - success`(filter: VisitModel.VisitFilter) {
        visitService.getAll(filter, clinicIdSet).test().assertNext {
            when (it.visitList.size) {
                0 -> assert(filter.fio == absentPerson.fio || filter.phone == absentPerson.phone || filter.petName == absentPet.name)
                1 -> assertAll(
                    { assertNotEquals(VisitModel.VisitFilter.getDefaultInstance(), filter) },
                    { assert(visitEntity1.id == UUID.fromString(it.visitList[0].uuid)) }
                )
                2 -> assertAll(
                    { assertEquals(VisitModel.VisitFilter.getDefaultInstance(), filter) },
                    { assert(visitEntity1.id == it.visitList[0].uuid.uuid() || visitEntity1.id == it.visitList[1].uuid.uuid()) },
                    { assert(visitEntity2.id == it.visitList[0].uuid.uuid() || visitEntity2.id == it.visitList[1].uuid.uuid()) }
                )
            }
        }.verifyComplete()
    }


    private fun visitFilterProvider() =
        listOf(
            VisitModel.VisitFilter.getDefaultInstance(),
            VisitModel.VisitFilter.newBuilder().apply { fio = person1.fio }.build(),
            VisitModel.VisitFilter.newBuilder().apply { phone = person1.phone }.build(),
            VisitModel.VisitFilter.newBuilder().apply { fio = person1.fio; phone = person1.phone }.build(),
            VisitModel.VisitFilter.newBuilder().apply { diagnosis = expectedDiagnosis }.build(),
            VisitModel.VisitFilter.newBuilder().apply { petName = pet1.name }.build(),
            VisitModel.VisitFilter.newBuilder().addAllState(listOf(VisitModel.State.CLOSED)).build(),
            VisitModel.VisitFilter.newBuilder().addAllDoctorId(listOf(doctor1.id)).build(),
            VisitModel.VisitFilter.newBuilder().apply {
                dateFrom = instantNow.minus(1, ChronoUnit.DAYS).epochSecond
                dateTo = instantNow.plus(1, ChronoUnit.DAYS).epochSecond
            }.build(),
            VisitModel.VisitFilter.newBuilder().apply { fio = absentPerson.fio }.build(),
            VisitModel.VisitFilter.newBuilder().apply { phone = absentPerson.phone }.build(),
            VisitModel.VisitFilter.newBuilder().apply { petName = absentPet.name }.build()
        )


    private fun initMock() {
        every { petClient.getByName(pet1.name) } returns petList.toMono()
        every { petClient.getByName("") } returns PetModel.PetList.getDefaultInstance().toMono()
        every { personClient.getByFioOrPhone(person1.fio, "") } returns personList.toMono()
        every { personClient.getByFioOrPhone("", person1.phone) } returns personList.toMono()
        every { personClient.getByFioOrPhone(person1.fio, person1.phone) } returns personList.toMono()
        every { personClient.getByFioOrPhone("", "") } returns PersonModel.PersonList.getDefaultInstance().toMono()
        every { personClient.getByFioOrPhone(absentPerson.fio, "") } returns PersonModel.PersonList.getDefaultInstance().toMono()
        every { petClient.getByName(absentPet.name) } returns PetModel.PetList.getDefaultInstance().toMono()
            .toMono()
        every {
            personClient.getByFioOrPhone(
                "",
                absentPerson.phone
            )
        } returns PersonModel.PersonList.getDefaultInstance().toMono()
        every { customVisitRepository.findAllByFilter(daoVisitFilterBuilder(stateList = listOf(VisitModel.State.CLOSED))) } returns Flux.fromIterable(
            listOf(visitEntity1)
        )
        every { customVisitRepository.findAllByFilter(daoVisitFilterBuilder(ownerId = setOf(UUID.fromString(person1.id)))) } returns Flux.fromIterable(
            listOf(visitEntity1)
        )
        every {
            customVisitRepository.findAllByFilter(
                daoVisitFilterBuilder(
                    dateFrom = instantNow.minus(1, ChronoUnit.DAYS).epochSecond,
                    dateTo = instantNow.plus(1, ChronoUnit.DAYS).epochSecond
                )
            )
        } returns Flux.fromIterable(listOf(visitEntity1))
        every {
            customVisitRepository.findAllByFilter(
                daoVisitFilterBuilder(
                    doctorIdList = listOf(
                        UUID.fromString(
                            doctor1.id
                        )
                    )
                )
            )
        } returns Flux.fromIterable(listOf(visitEntity1))
        every { customVisitRepository.findAllByFilter(daoVisitFilterBuilder(diagnosis = expectedDiagnosis)) } returns Flux.fromIterable(
            listOf(visitEntity1)
        )
        every { customVisitRepository.findAllByFilter(daoVisitFilterBuilder(petId = setOf(UUID.fromString(pet1.id)))) } returns Flux.fromIterable(
            listOf(visitEntity1)
        )
        every { customVisitRepository.findAllByFilter(daoVisitFilterBuilder()) } returns Flux.fromIterable(
            listOf(
                visitEntity1,
                visitEntity2
            )
        )
        every { petClient.getByIds(setOf(UUID.fromString(pet1.id))) } returns PetModel.PetList.newBuilder()
            .addAllPet(listOf(pet1))
            .build().toMono()
        every { personClient.getByIds(setOf(UUID.fromString(person1.id))) } returns PersonModel.PersonList.newBuilder()
            .addAllPerson(listOf(person1))
            .build().toMono()
        every { userClient.getByIds(setOf(UUID.fromString(doctor1.id))) } returns UserModel.UserList.newBuilder()
            .addAllUser(listOf(doctor1))
            .build().toMono()
    }

    private fun daoVisitFilterBuilder(
        stateList: List<VisitModel.State> = emptyList(),
        ownerId: Set<UUID> = emptySet(),
        dateFrom: Long = 0,
        dateTo: Long = 0,
        doctorIdList: List<UUID> = emptyList(),
        diagnosis: String = "",
        petId: Set<UUID> = emptySet(),
        clinicIds: Set<UUID> = emptySet()
    ) = DaoVisitFilter(
        stateList,
        ownerId,
        dateFrom,
        dateTo,
        doctorIdList,
        diagnosis,
        petId,
        clinicIds.takeIf { it.isNotEmpty() } ?: clinicIdSet)
}
