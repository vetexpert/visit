package com.vetexpert.visit

import com.vetexpert.proto.person.PersonModel
import com.vetexpert.proto.pet.PetModel
import com.vetexpert.proto.user.UserModel
import com.vetexpert.proto.visit.VisitModel
import com.vetexpert.utils.uuid
import com.vetexpert.visit.model.VisitEntity
import com.vetexpert.visit.model.enums.State
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.apache.commons.lang3.RandomStringUtils.randomNumeric
import java.util.*
import kotlin.random.Random

val person1 = PersonModel.Person.newBuilder().apply {
    id = UUID.randomUUID().toString()
    phone = randomNumeric(10)
    fio = randomAlphabetic(50)
    address = randomAlphabetic(50)
    bdate = Date().time
    sex = randomAlphabetic(1)
    docNumber = randomNumeric(10)
    email = randomAlphabetic(20)
}.build()

val personList = PersonModel.PersonList.newBuilder().addPerson(person1).build()

val absentPerson = PersonModel.Person.newBuilder().apply {
    id = UUID.randomUUID().toString()
    phone = randomNumeric(10)
    fio = randomAlphabetic(50)
    address = randomAlphabetic(50)
    bdate = Date().time
    sex = randomAlphabetic(1)
    docNumber = randomNumeric(10)
    email = randomAlphabetic(20)
}.build()

val pet1 = PetModel.Pet.newBuilder().apply {
    id = UUID.randomUUID().toString()
    type = randomAlphabetic(10)
    kind = randomAlphabetic(10)
    name = randomAlphabetic(10)
    bdate = Date().time
    sex = randomAlphabetic(1)
    owner = person1
}.build()

val petList = PetModel.PetList.newBuilder().addPet(pet1).build()

val absentPet = PetModel.Pet.newBuilder().apply {
    id = UUID.randomUUID().toString()
    type = randomAlphabetic(10)
    kind = randomAlphabetic(10)
    name = randomAlphabetic(10)
    bdate = Date().time
    sex = randomAlphabetic(1)
    owner = absentPerson
}.build()

val doctor1 = UserModel.User.newBuilder().apply {
    id = UUID.randomUUID().toString()
    fio = randomAlphabetic(10)
    avatar = randomAlphabetic(10)
    position = randomAlphabetic(10)
    phoneNumber = randomNumeric(10)
}
    .addAllRole(listOf(randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(5)))
    .build()

val visitEntity1 = VisitEntity(
    id = UUID.randomUUID(),
    doctorId = doctor1.id.uuid(),
    ownerId = person1.id.uuid(),
    petId = pet1.id.uuid(),
    anamnesis = randomAlphabetic(10),
    checkup = randomAlphabetic(10),
    temporaryDiagnosis = randomAlphabetic(10),
    finallyDiagnosis = randomAlphabetic(10),
    recommendations = randomAlphabetic(10),
    date = Date().time,
    state = State.CLOSED,
    number = Random.nextLong(),
    clinicId = UUID.randomUUID()
)

val visitEntity2 = VisitEntity(
    id = UUID.randomUUID(),
    doctorId = doctor1.id.uuid(),
    ownerId = person1.id.uuid(),
    petId = pet1.id.uuid(),
    anamnesis = randomAlphabetic(10),
    checkup = randomAlphabetic(10),
    temporaryDiagnosis = randomAlphabetic(10),
    finallyDiagnosis = randomAlphabetic(10),
    recommendations = randomAlphabetic(10),
    date = Date().time,
    state = State.CLOSED,
    number = Random.nextLong(),
    clinicId = UUID.randomUUID()
)

val visitProto: VisitModel.Visit = VisitModel.Visit.newBuilder()
    .apply {
        uuid = UUID.randomUUID().toString()
        anamnesis = randomAlphabetic(10)
        checkup = randomAlphabetic(10)
        temporaryDiagnosis = randomAlphabetic(10)
        finallyDiagnosis = randomAlphabetic(10)
        recommendations = randomAlphabetic(10)
        date = Date().time
        state = VisitModel.State.CLOSED
        number = Random.nextLong()
        owner = PersonModel.Person.getDefaultInstance()
        pet = PetModel.Pet.getDefaultInstance()
        doctor = UserModel.User.getDefaultInstance()
    }
    .addManipulation(VisitModel.Manipulation.getDefaultInstance())
    .build()

val visitListProto: VisitModel.VisitList = VisitModel.VisitList.newBuilder()
    .addVisit(visitProto)
    .build()
