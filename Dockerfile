FROM adoptopenjdk/openjdk11:armv7l-ubuntu-jre11u-2021-10-15-03-58-beta-nightly
COPY build/libs/visit-*.jar ./app/app.jar
CMD ["java", "-jar", "./app/app.jar"]
